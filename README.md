## Code Test for CASAiQ by Justin Tanner

The following is a code test for CasaiQ based off the instructions at:

https://gist.github.com/techgaun/bab97d24cf4084d07a877f980b829e7d

#### Install

```
git clone git@bitbucket.org:justintanner/casaiq-code-test.git
yarn 
```

#### Running the server

```
yarn start
```

#### Tests

```
yarn test
```

#### Notes

This was a fun code test. It gave me a chance to look into React Redux for the first time.

Let me know what you think of redux code.
 
 Thanks, Justin.