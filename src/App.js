import React, { Component } from "react";
import { Route, NavLink, BrowserRouter } from "react-router-dom";
import logo from "./logo.png";
import AboutUs from "./AboutUs";
import Home from "./Home";
import Device from "./Device";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <header>
            <NavLink exact to="/">
              <img src={logo} alt="logo" height="30" />
            </NavLink>
            <ul className="nav">
              <li>
                <NavLink to="/about-us">About Us</NavLink>
              </li>
            </ul>
            <div className="clear" />
          </header>
          <div className="content">
            <Route exact path="/" component={Home} />
            <Route path="/about-us" component={AboutUs} />
            <Route path="/device/:slug" component={Device} />
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
