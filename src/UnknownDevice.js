import React, { Component } from "react";

class UnknownDevice extends Component {
  render() {
    return (
      <div>
        <h1>Unknown Device</h1>
        <p>Sorry, We are not supporting this device at the moment.</p>
      </div>
    );
  }
}

export default UnknownDevice;
