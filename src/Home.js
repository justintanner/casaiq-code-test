import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

class Home extends Component {
  render() {
    let lockLinks = this.props.locks.map(lock => {
      return (
        <li>
          <Link to={`/device/${lock.slug}`}>{lock.title}</Link>
        </li>
      );
    });

    return (
      <div>
        <h1>Active Devices</h1>
        <ul className="lock-links">{lockLinks}</ul>
      </div>
    );
  }
}

function mapState(state) {
  return { locks: state };
}

export default connect(mapState, null)(Home);
