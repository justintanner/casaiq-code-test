import React from "react";
import { render } from "enzyme";
import UnknownDevice from "./UnknownDevice";

it("renders without crashing", () => {
  render(<UnknownDevice />);
});
