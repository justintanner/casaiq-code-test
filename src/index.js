import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore } from "redux";
import lockStore from "./lockStore";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";

let initialLocks = [
  {
    type: "lock",
    state: "locked",
    last_updated_at: 1508386138,
    slug: "apt-143-lock",
    title: "Apt 123 Lock",
    history: []
  },
  {
    type: "lock",
    state: "unlocked",
    last_updated_at: 1508386137,
    slug: "apt-999-lock",
    title: "Apt 999 Lock",
    history: []
  }
];

let store = createStore(lockStore, initialLocks);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);

registerServiceWorker();
