import lockStore from "./lockStore";

it("adds a new lock", () => {
  const state = lockStore([], { type: "ADD_LOCK", slug: "new-444-lock" });

  expect(state[0]).toHaveProperty("type", "lock");
  expect(state[0]).toHaveProperty("slug", "new-444-lock");
  expect(state[0]).toHaveProperty("title", "New 444 Lock");
  expect(state[0]).toHaveProperty("state", "locked");
  expect(state[0].last_updated_at).toBeGreaterThan(0);
  expect(state[0].history[0]).toHaveProperty("state", "locked");
  expect(state[0].history[0].last_updated_at).toBeGreaterThan(0);
});

it("does not add duplicate locks", () => {
  const state = lockStore([{ slug: "existing-lock" }], {
    type: "ADD_LOCK",
    slug: "existing-lock"
  });

  expect(state.length).toEqual(1);
});

it("toggles a lock", () => {
  let state = lockStore([], { type: "ADD_LOCK", slug: "lock" });

  expect(state[0].state).toEqual("locked");

  state = lockStore(state, { type: "TOGGLE_LOCK", slug: "lock" });

  expect(state[0].state).toEqual("unlocked");
});

it("maintains a history lock toggles", () => {
  let state = lockStore([], { type: "ADD_LOCK", slug: "lock" });

  expect(state[0].history[0].state).toEqual("locked");
  const firstTimestamp = state[0].history[0].last_updated_at;

  state = lockStore(state, { type: "TOGGLE_LOCK", slug: "lock" });

  setTimeout(() => {
    expect(state[0].history[1].state).toEqual("unlocked");
    const secondTimestamp = state[0].history[1].last_updated_at;

    console.log(state[0].history);
    expect(secondTimestamp).toBeGreaterThan(firstTimestamp);
  }, 1000);
});

it("only stores the last 10 lock toggles", () => {
  let state = lockStore([], { type: "ADD_LOCK", slug: "lock" });

  for (let i = 0; i < 20; i++) {
    state = lockStore(state, { type: "TOGGLE_LOCK", slug: "lock" });
  }

  expect(state[0].history.length).toEqual(10);
});
