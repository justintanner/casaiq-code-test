import React from "react";
import { render } from "enzyme";
import { createStore } from "redux";
import lockStore from "./lockStore";
import Home from "./Home";

it("renders without crashing", () => {
  let store = createStore(lockStore);

  render(<Home store={store} />);
});
