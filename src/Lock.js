import React, { Component } from "react";
import _ from "underscore";
import { connect } from "react-redux";
import lockedIcon from "./locked.svg";
import unlockedIcon from "./unlocked.svg";

class Lock extends Component {
  componentWillMount() {
    // Create a lock from any valid lock url
    if (!_.findWhere(this.props.locks, { slug: this.props.slug })) {
      // This dispatch will trigger a re-render of this component so render can use the newly created lock.
      this.props.add(this.props.slug);
    }
  }

  render() {
    const lock = _.findWhere(this.props.locks, { slug: this.props.slug });
    let locked = lock.state === "locked";

    return (
      <div className="lock">
        <h1>{lock.title}</h1>

        <img
          className="lock-icon"
          alt="lock icon"
          src={locked ? lockedIcon : unlockedIcon}
        />

        <br />

        <button
          type="button"
          className="btn"
          onClick={() => {
            console.log("hai");
            this.props.toggle(this.props.slug);
          }}
        >
          {locked ? "Unlock" : "Lock"}
        </button>
      </div>
    );
  }
}

function mapState(state) {
  return { locks: state };
}

function mapDispatch(dispatch) {
  return {
    add: slug => {
      dispatch({ type: "ADD_LOCK", slug: slug });
    },
    toggle: slug => {
      dispatch({ type: "TOGGLE_LOCK", slug: slug });
    }
  };
}

export default connect(mapState, mapDispatch)(Lock);
