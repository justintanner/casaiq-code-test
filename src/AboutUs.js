import React, { Component } from "react";

class AboutUs extends Component {
  render() {
    return (
      <div>
        <h1>About Us</h1>
        <p>
          CasaIQ - A Smart Home Solution Custom-built for Apartments, Condos,
          and Multifamily Real Estate.
        </p>
      </div>
    );
  }
}

export default AboutUs;
