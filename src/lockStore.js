import _ from "underscore";

function lockStore(state = [], action) {
  switch (action.type) {
    case "ADD_LOCK":
      if (!_.findWhere(state, { slug: action.slug })) {
        const now = Date.now();

        state.push({
          type: "lock",
          state: "locked",
          last_updated_at: now,
          slug: action.slug,
          title: capitalize(action.slug),
          history: [
            {
              state: "locked",
              last_updated_at: now
            }
          ]
        });
      }

      return state.slice();
    case "TOGGLE_LOCK":
      let lock = _.findWhere(state, { slug: action.slug });

      if (lock.state === "locked") {
        lock.state = "unlocked";
      } else {
        lock.state = "locked";
      }

      const now = Date.now();
      lock.last_updated_at = now;

      lock.history.push({
        state: lock.state,
        last_updated_at: now
      });

      if (lock.history.length > 10) {
        lock.history.shift();
      }

      return state.slice();
    default:
      return state;
  }
}

function capitalize(string) {
  let title = string.toLowerCase().replace(/-/g, " ");

  return title.replace(/\w\S*/g, word => {
    return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
  });
}

export default lockStore;
