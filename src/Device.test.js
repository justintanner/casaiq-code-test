import React from "react";
import { render, shallow } from "enzyme";
import Device from "./Device";

it("renders without crashing", () => {
  const mockedMatch = { params: { slug: "apt-123-lock" } };

  const div = document.createElement("div");
  shallow(<Device match={mockedMatch} />, div);
});

it("renders unknown device when given a non lock device", () => {
  const mockedMatch = { params: { slug: "rubber-ducky" } };

  const div = document.createElement("div");
  const device = render(<Device match={mockedMatch} />, div);

  expect(device.find("h1").text()).toEqual("Unknown Device");
});
