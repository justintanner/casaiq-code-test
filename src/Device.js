import React, { Component } from "react";
import Lock from "./Lock";
import UnknownDevice from "./UnknownDevice";

class Device extends Component {
  render() {
    const slug = this.props.match.params.slug;
    const isLock = slug.endsWith("lock");

    return <div>{isLock ? <Lock slug={slug} /> : <UnknownDevice />}</div>;
  }
}

export default Device;
