import React from "react";
import { render } from "enzyme";
import { createStore } from "redux";
import lockStore from "./lockStore";
import Lock from "./Lock";

let store;

beforeEach(() => {
  store = createStore(lockStore);
});

it("renders without crashing", () => {
  render(<Lock slug="apt-123-lock" store={store} />);
});

it("capitalizes the title of the device", () => {
  const lock = render(<Lock slug="apt-123-lock" store={store} />);

  expect(lock.find("h1").text()).toEqual("Apt 123 Lock");
});

// Sadly this is not work once I switched to Redux, not sure why.
/*it("toggles between locked and unlocked", () => {
  const lock = render(<Lock slug="apt-123-lock" store={store} />);

  let button = lock.find(".btn");
  expect(button.text()).toEqual("Unlock");

  button.simulate('click');

  button = lock.find("button");
  expect(button.text()).toEqual("Lock");
});*/
